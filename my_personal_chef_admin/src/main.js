import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import router from './router'
import firebase from "firebase";


Vue.config.productionTip = false

let firebaseConfig = {
  apiKey: "AIzaSyAT2kqzOTKONl1-J9PqySY41LXPdz0toho",
  authDomain: "mypersonalchef-22f20.firebaseapp.com",
  projectId: "mypersonalchef-22f20",
  storageBucket: "mypersonalchef-22f20.appspot.com",
  messagingSenderId: "960299907745",
  appId: "1:960299907745:web:78c2b9d4fa22a473490ee0",
  measurementId: "G-JE7WXL937R"
};
// Initialize Firebase

firebase.initializeApp(firebaseConfig);


new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
