import firebase from "firebase";


export let service = {
    getRecipesCollection(){
     return firebase.firestore().collection("recipes");
    },

    getTagsCollection(){
        return firebase.firestore().collection("tags");
    },

    getTags(){
        return this.getTagsCollection().get();
    },
    mergeTags(tags,index){
        return this.getTagsCollection().doc(index).update(tags);
    },
    addTags(tags){
        return this.getTagsCollection().doc('tags').update(tags);
    },
    // getTagsByIdRecipes(idRecipe){
    //   return this.getRecipesCollection().doc(idRecipe).collection("tags").get()
    // },
    getStepsByIdRecipes(idRecipe){
      return this.getRecipesCollection().doc(idRecipe).collection("steps").get()
    },
    getIngredientsByIdRecipes(idRecipe){
        return this.getRecipesCollection().doc(idRecipe).collection("ingredients").get()
    },


    addRecipe(recipe){
        let db = firebase.firestore();
        // eslint-disable-next-line no-async-promise-executor
        return new Promise((async (resolve, reject) => {
            let ingredients = recipe.ingredients;
            let steps = recipe.steps;
            let tags = recipe.tags;
            try {
            this.getRecipesCollection().add({
                title: recipe.title,
                glutenFree: recipe.glutenFree,
                meetFree: recipe.meetFree,
                milkFree: recipe.milkFree,
                time: recipe.time,
                portions: recipe.portions,
                tags:tags,
            }).then(doc=>{
                // let tagsCollection = doc.collection('tags');
                let stepsCollection = doc.collection('steps');
                let ingredientsCollection = doc.collection('ingredients');

                // Begin a new batch
                let batch = db.batch();

                // Set each document, as part of the batch
                // tags.forEach(t => {
                //     let ref = tagsCollection.doc(t);
                //     batch.set(ref,{id:t});
                // })
                steps.forEach((a, index) => {
                    let ref = stepsCollection.doc(index.toString());
                    batch.set(ref,{order:index,data:a});
                })
                ingredients.forEach(a=> {
                    let ref = ingredientsCollection.doc(a.ingredient);
                    batch.set(ref,a);
                })
                // Commit the entire batch
                 let promise = batch.commit();
            resolve(promise);
            });

            }catch (e) {reject();}
        }));

    },
    mergeRecipe(recipe){
        let db = firebase.firestore();
        // eslint-disable-next-line no-async-promise-executor
        return new Promise((async (resolve, reject) => {
            let ingredients = recipe.ingredients;
            let steps = recipe.steps;
            let tags = recipe.tags;
            try {
                this.getRecipesCollection().add({
                    title: recipe.title,
                    glutenFree: recipe.glutenFree,
                    meetFree: recipe.meetFree,
                    milkFree: recipe.milkFree,
                    time: recipe.time,
                    portions: recipe.portions,
                    tags:tags
                }).then(doc=>{
                    // let tagsCollection = doc.collection('tags');
                    let stepsCollection = doc.collection('steps');
                    let ingredientsCollection = doc.collection('ingredients');

                    // Begin a new batch
                    let batch = db.batch();

                    // Set each document, as part of the batch
                    // tags.forEach(t => {
                    //     let ref = tagsCollection.doc(t);
                    //     batch.update(ref,{id:t});
                    // })
                    steps.forEach((a, index) => {
                        let ref = stepsCollection.doc(index.toString());
                        batch.update(ref,{order:index,data:a});
                    })
                    ingredients.forEach(a=> {
                        let ref = ingredientsCollection.doc(a.ingredient);
                        batch.update(ref,a);
                    })
                    // Commit the entire batch
                    let promise = batch.commit();
                    resolve(promise);
                });

            }catch (e) {reject();}
        }));
        // return this.getRecipesCollection().doc(id).update(recipe);
    },

    getAllRecipe(){
        return this.getRecipesCollection().get();
    },
    deleteRecipe(item){
        return this.getRecipesCollection().doc(item.id).delete();
    }





}