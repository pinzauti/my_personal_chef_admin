export class Recipe {
    constructor(id, title, time, portions, milkFree, meetFree, glutenFree, tags, steps, ingredients) {
        this.id = id;
        this.title = title;
        this.time = time;
        this.portions = portions;
        this.milkFree = milkFree;
        this.meetFree = meetFree;
        this.glutenFree = glutenFree;
        this.tags = tags;
        this.steps = steps;
        this.ingredients = ingredients;
    }
}

